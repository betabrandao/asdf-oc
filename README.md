= asdf-oc

[![pipeline status](https://gitlab.com/betabrandao/asdf-oc/badges/main/pipeline.svg)](https://gitlab.com/betabrandao/asdf-oc/-/commits/main)

Fork of original project: https://github.com/bartoszmajsak/asdf-oc

https://github.com/openshift/origint[Openshift] plugin for https://github.com/asdf-vm/asdf[asdf] version manager.

== Install

[source,bash]
----
asdf plugin-add oc https://gitlab.com/betabrandao/asdf-oc.git
----

== Use

Check out the https://github.com/asdf-vm/asdf#asdf-[asdf README] for instructions on how to install and manage versions of variety of tools it supports.
